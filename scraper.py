from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.parse import urlencode
from urllib.request import Request
import time

base_url = "http://osoc.berkeley.edu/OSOC/osoc?y=0&p_term=SP&p_deptname={0}&p_course={1}"

def gen_url(department, course):
    return base_url.format(department, course)

def return_live_course_info(ccn):
    try:
        url = "https://telebears.berkeley.edu/enrollment-osoc/osc"
        values = {"_InField1":"RESTRIC", "_InField2":ccn, "_InField3":"13b4"}
        data = urlencode(values)
        data = bytearray(data, "utf-8")
        req = Request(url, data)
        response = urlopen(req)
        html = response.read().decode()
        soup = BeautifulSoup(html)
        results = str(soup.find("blockquote").find("div").text).strip().split("\n")
        courseSizeInfo = results[0].split()
        waitlistSizeInfo = results[2].split()
        return {'class size':courseSizeInfo[-1], 'current size':courseSizeInfo[0], \
                'waitlist limit': waitlistSizeInfo[-1], 'waitlist size': waitlistSizeInfo[0]}
    except Exception:
        return "None"

def scrape_course_info(url):
    response = urlopen(url)
    html = response.read().decode()
    soup = BeautifulSoup(html)
    output_dict = {}
    search_results = soup.find_all("table")[1:-1]
    for each in search_results:
        class_details = (str)(each.text).strip().replace("\xc2", "").replace("\xa0", "").split("\n")
        class_details_dict = {elem.split(":")[0].lower():elem.split(":")[1].lower() for elem in class_details}
        class_details_dict['enrollment'] = return_live_course_info(class_details_dict['course control number'])
        output_dict[class_details_dict['course']] = class_details_dict
        #print(class_details_dict)
        return output_dict

old_course_info = scrape_course_info(gen_url("Computer+Science","61B"))
while True:
    new_course_info = scrape_course_info(gen_url("Computer+Science","61B"))
    
    if new_course_info != old_course_info:
        print("Class changed!")
    else:
        print("Class didn't change")

    time.sleep(5)

#print(return_live_course_info(26274))
    
